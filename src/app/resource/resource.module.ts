import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { ResourcePageRoutingModule } from './resource.router.module';

import { ResourcePage } from './resource.page';

const routes: Routes = [
  {
    path: '',
    component: ResourcePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResourcePageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ResourcePage]
})
export class ResourcePageModule {}
